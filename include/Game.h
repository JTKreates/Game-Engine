#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include "State/StateManager.h"

class Game
{
    public:
        Game();
        virtual ~Game();

        void init(std::string title, uint width, uint height, bool isFullscreen = false);
        void close();

        bool isRunning() {return m_running;}

        void update();
        void render();
        void handleEvents();

    protected:

    private:
        bool m_running = false;

        sf::RenderWindow m_window;
        sf::Event m_event;
};

#endif // GAME_H
