#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <map>

class ResourceManager
{
    public:
        static ResourceManager &getResource();
        virtual ~ResourceManager();

        void loadResources();

        std::map<std::string, sf::Texture> textureMap;
        std::map<std::string, sf::Sound> soundMap;
        std::map<std::string, sf::Font> fontMap;

    protected:

    private:
        ResourceManager();

        std::map<std::string, sf::SoundBuffer> soundBufferMap;

        void loadTexture(std::string key, std::string filename);
        void loadSound(std::string key, std::string filename);
        void loadFont(std::string key, std::string filename);
};

#endif // RESOURCEMANAGER_H
