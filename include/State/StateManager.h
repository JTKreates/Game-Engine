#ifndef STATEMANAGER_H
#define STATEMANAGER_H

#include <SFML/Graphics.hpp>
#include "State/State.h"
#include "State/Load.h"

class StateManager
{
    public:
        static StateManager &getState();
        virtual ~StateManager();

        void createState(State *state);

        void load();
        void unload();

        void render(sf::RenderWindow &window);
        void handleEvents(sf::Event &event);

    protected:

    private:
        StateManager();
        StateManager(StateManager const&);
        void operator=(StateManager const&);

        State *currentState, *newState;
};

#endif // STATEMANAGER_H
