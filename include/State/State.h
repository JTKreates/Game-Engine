#ifndef STATE_H
#define STATE_H

#include <SFML/Graphics.hpp>

class State
{
    public:
        State();
        virtual ~State();

        virtual void load();
        virtual void unload();

        virtual void render(sf::RenderWindow &window);
        virtual void handleEvents(sf::Event &event);

    protected:

    private:
};

#endif // STATE_H
