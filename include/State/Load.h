#ifndef LOAD_H
#define LOAD_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include "State/State.h"

class Load : public State
{
    public:
        Load();
        virtual ~Load();

        void load();
        void unload();

        void render(sf::RenderWindow &window);
        void handleEvents(sf::Event &event);

    protected:

    private:
};

#endif // LOAD_H
