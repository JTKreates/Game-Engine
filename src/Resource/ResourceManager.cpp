#include "Resource/ResourceManager.h"

ResourceManager &ResourceManager::getResource()
{
    static ResourceManager rm;
    return rm;
}

ResourceManager::ResourceManager()
{}

ResourceManager::~ResourceManager()
{}

void ResourceManager::loadTexture(std::string key, std::string filename)
{
    sf::Texture texture;

    if(!texture.loadFromFile("res/gfx/" + filename))
        std::cout << "Failed to load: res/gfx/" << filename << std::endl;
    else
        textureMap[key] = texture;
}

void ResourceManager::loadSound(std::string key, std::string filename)
{
    sf::SoundBuffer soundBuffer;

    if(!soundBuffer.loadFromFile("res/sfx/" + filename))
        std::cout << "Failed to load: res/sfx/" << filename << std::endl;
    else
    {
        soundBufferMap[key] = soundBuffer;
        soundMap[key] = sf::Sound(soundBufferMap[key]);
    }
}

void ResourceManager::loadFont(std::string key, std::string filename)
{
    sf::Font font;

    if(!font.loadFromFile("res/font/" + filename))
        std::cout << "Failed to load: res/font/" << filename << std::endl;
    else
        fontMap[key] = font;
}
