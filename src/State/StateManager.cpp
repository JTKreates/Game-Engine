#include "StateManager.h"

StateManager &StateManager::getState()
{
    static StateManager sm;
    return sm;
}

StateManager::StateManager()
{
    currentState = new Load;
}

StateManager::~StateManager()
{}

void StateManager::createState(State *state)
{
    currentState->unload();
    delete currentState;
    currentState = state;
    currentState->load();
}

void StateManager::load()
{
    currentState->load();
}

void StateManager::unload()
{
    currentState->unload();
}

void StateManager::render(sf::RenderWindow &window)
{
    currentState->render(window);
}

void StateManager::handleEvents(sf::Event &event)
{
    currentState->handleEvents(event);
}
