#include "Load.h"

Load::Load()
{
    std::cout << "Loaded \"Load\" State" << std::endl;
}

Load::~Load()
{}

void Load::load()
{}

void Load::unload()
{}

void Load::render(sf::RenderWindow &window)
{}

void Load::handleEvents(sf::Event &event)
{}
