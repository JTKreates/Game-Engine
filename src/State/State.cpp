#include "State.h"

State::State()
{}

State::~State()
{}

void State::load()
{}

void State::unload()
{}

void State::render(sf::RenderWindow &window)
{}

void State::handleEvents(sf::Event &event)
{}
