#include "Game.h"

int main()
{
    Game game;

    game.init("My Window", 640, 480);

    while(game.isRunning())
    {
        game.update();
    }

    return 0;
}
