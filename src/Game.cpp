#include "Game.h"

Game::Game()
{}

Game::~Game()
{}

void Game::init(std::string title, uint width, uint height, bool isFullscreen)
{
    m_window.create(sf::VideoMode(width, height), title, isFullscreen ? sf::Style::Fullscreen : sf::Style::Default);
    if(m_window.isOpen())
        m_running = true;
}

void Game::close()
{
    m_running = false;
    m_window.close();
}

void Game::update()
{
    while(m_window.isOpen())
    {
        while(m_window.pollEvent(m_event))
        {
            handleEvents();
        }

        render();
    }
}

void Game::render()
{
    m_window.clear(sf::Color::White);

    StateManager::getState().render(m_window);

    m_window.display();
}

void Game::handleEvents()
{
    if(m_event.type == sf::Event::Closed)
        close();
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        close();

    StateManager::getState().handleEvents(m_event);
}
